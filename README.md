# CC Parse

Current status: **in development**.

[![Pipeline status](https://gitlab.com/nkipling/cc-parse/badges/master/pipeline.svg)](https://gitlab.com/nkipling/cc-parse/-/commits/master)

A [Conventional Comments](https://conventionalcomments.org/) parser for Javascript.

See a demo of this [package in action](https://parser-demo.netlify.app/).

This is a simple package that exposes a function which can parse a conventional
comment string into a JSON object. For example, it will take the comment:

> **question (non-blocking):** At this point, does it matter which thread has won?
>
> Maybe to prevent a race condition we should keep looping until they've all won?

and turn this into:

```json
{
  "label": "question",
  "decoration": "non-blocking",
  "subject": "At this point, does it matter which thread has won?",
  "discussion": [
    "Maybe to prevent a race condition we should keep looping until they've all won?",
  ]
}
```

Allowing you to do anything with the resulting object. :)

## How does it work?

The parser expects the string passed in to match the Conventional Comments
specification. If the string doesn't, then the parser will return either `null`
or optionally can `throw`.

The resulting object will look like:

```typescript
interface ParseResult {
  label: 'chore' | 'issue' | 'nitpick' | 'praise' | 'question' | 'suggestion' | 'thought';
  decoration: string | '';
  subject: string;
  discussion: string[];
}
```
Where:

* `label` should always match one of the specified labels in Conventional Commits
* `decoration`: can either be a supplied decoration or an empty string
* `subject` is always required
* `discussion` will always be an array of strings or an empty array

## Getting Started

TODO: This is not hosted on any package registries yet, to be done. For now, you
can clone this repo and run `npm pack`, then `npm install path-to-the-tgz`.

Once installed in your project, you can use it like so:

```javascript
import parse from 'cc-parse';

const myString = `**thought (non-blocking):** Is it worth grouping these imports?`

const result = parse(myString);

// Do something with your result, e.g. console.log(result);
```

## Misc

The parsing uses [nearley.js](https://nearley.js.org/) and this package includes
a grammar that describes how to parse the comments and a function that wraps 
this with some other checks. I am by no means a nearley expert (this is the first
time I've ever used it), so I suspect the grammar is ripe for improvements, bug 
fixes and optimisations - some of which I hope to tackle as I learn more.

As of right now, it works for basic comment formats, including some basic markdown
variations, but it hasn't been extensively tested yet so I'm sure there's problems.

My goal is to use this to add more features to [Tal Tal](https://gitlab.com/nkipling/tal-tal-new).

## Building

This is a simple project but it still uses Typescript and Webpack. Simply clone
the repo then:

```
yarn install
```

To run tests:

```
yarn test
```

To build:

```
yarn build
```

This will produce a `dist` folder with the necessary files.
