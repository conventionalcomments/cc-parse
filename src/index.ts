import { Grammar, Parser } from 'nearley';
import * as grammar from './grammar/grammar';
import { ParseResult } from './models';

/**
 * Parses a string and extracts to a ParseResult if the string can be parsed as
 * a conventional comment or returns null (or optionally can throw).
 * @param str String to parse
 * @param throwErr Throw on failure to parse (default false)
 */
const parse = (str: string | string[], throwErr: boolean = false): ParseResult | null => {
  const parser = new Parser(Grammar.fromCompiled(grammar));

  if (!str || str.length === 0) {
    throw new Error('Pass a string or array of strings');
  }

  try {
    if (str instanceof Array) {
      // TODO: should we parse each arr element into a string?
      str.forEach((x) => parser.feed(x));
    } else {
      parser.feed(str);
    }
  } catch (ex) {
    if (throwErr) {
      throw new Error(ex);
    } else {
      return null;
    }
  }

  if (parser.results.length > 1) {
    console.warn(`Parser results are ambiguous, found ${parser.results.length} ways to parse.`);
  } else if (parser.results.length === 0) {
    if (throwErr) {
      throw new Error('Unable to parse');
    } else {
      return null;
    }
  }

  const [result] = parser.results as ParseResult[];

  // Only return if we have a valid subject
  if (result.subject) {
    return result;
  } else {
    if (throwErr) {
      throw new Error('Unable to parse');
    } else {
      return null;
    }
  }
};

export default parse;
