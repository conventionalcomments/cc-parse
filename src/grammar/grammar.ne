# The grammar file for conventional: comments
#
# This describes the rules for parsing conventional comments into a data
# structure which can be used for anything!
#
# This is a nearley grammar file and the below is the grammar used to describe
# the syntax of a conventional comment. See: https://nearley.js.org/ for how
# this works.

# Import other defined syntax
@builtin "whitespace.ne"



# === Helper Functions === #

# These are small JS functions that are applied on the resulting syntax match

@{%
const flatDeep = (arr, d = 1) =>
  d > 0 ? arr.reduce((acc, val) => acc.concat(Array.isArray(val) ? flatDeep(val, d - 1) : val), [])
        : arr.slice();

const appendItemChar = (a, b) => { return (d) => d[a].concat(d[b]) };
const empty = (d) => [];
const emptyStr = (d) => "";

const commentParser = (d) => {
  return {
    "label": d[0][0],
    "decoration": d[2][0],
    "subject": d[3][0].trim(),
    "discussion": d[4] ? d[4].flat().reduce((acc, cur) => cur ? [...acc, cur] : acc, []) : []
  }
};

const markdownParser = (d) => {
  const flat = flatDeep(d, Infinity);
  const decoration = (flat.length <= 2 ? '' : flat[1]);
  const subject = (flat.length === 2 ? flat[1] : flat[2]) || '';
  const discussion = flat.slice(3).filter(d => d);

  return {
    "label": flat[0],
    "decoration": decoration,
    "subject": subject.trim(),
    "discussion": discussion,
  };
}
%}



# === Main === #

# This describes the general outline for how a comment can look
comment          -> L _ decoration S DI                             {% commentParser %}
                  | LM S DI                                         {% markdownParser %}



# === Labels === #

# Possible label values
L                -> "chore"i
                  | "issue"i
                  | "nitpick"i
                  | "praise"i
                  | "question"i
                  | "suggestion"i
                  | "thought"i

# Attempts to parse when wrapped in markdown
LM               -> MD L _ decoration                               {% d => [d[1][0], d[3][0]] %}
                  | MD L MD _ decoration                            {% d => [d[1], d[4]] %}

# Supported markdown characters
MD               -> BC                                              {% emptyStr %}
                  | IC                                              {% emptyStr %}

BC -> "**"
IC -> "__"



# === Decorators === #

# Determine how a decoration is presented
decoration       -> colon
                  | "(" _ D _ ")" colon                             {% d => d[2] %}
                  | "(" _ D _ ")" MD colon                          {% d => d[2] %}
                  | "(" _ D _ ")" colon MD                          {% d => d[2] %}
                  | MD "(" _ D _ ")" colon MD                       {% d => d[3] %}
                  | MD "(" _ D _ ")" MD colon                       {% d => d[3] %}

# Determine the possible decorator values
D                -> "non-blocking"



# === Subject === #

# This can be an any length string but is broken on a newline.
S                -> single_line



# === Discussion === #

DI               -> (newline para):*                                {% id %}



# === General === #

# Matchers used for general things like empty spaces, colons, paragraphs, etc.

single_line      -> para char                                       {% appendItemChar(0,1) %}

para             -> null                                            {% emptyStr %}
                  | para char                                       {% appendItemChar(0,1) %}

char             -> [^*\n|*\r]                                      {% id %}

newline          -> "\r" "\n"                                       {% emptyStr %}
                  | "\r" | "\n"                                     {% emptyStr %}

colon            -> ":"                                             {% emptyStr %}
