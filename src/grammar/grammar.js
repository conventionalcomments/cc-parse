// Generated automatically by nearley, version 2.19.2
// http://github.com/Hardmath123/nearley
(function () {
function id(x) { return x[0]; }

const flatDeep = (arr, d = 1) =>
  d > 0 ? arr.reduce((acc, val) => acc.concat(Array.isArray(val) ? flatDeep(val, d - 1) : val), [])
        : arr.slice();

const appendItemChar = (a, b) => { return (d) => d[a].concat(d[b]) };
const empty = (d) => [];
const emptyStr = (d) => "";

const commentParser = (d) => {
  return {
    "label": d[0][0],
    "decoration": d[2][0],
    "subject": d[3][0].trim(),
    "discussion": d[4] ? d[4].flat().reduce((acc, cur) => cur ? [...acc, cur] : acc, []) : []
  }
};

const markdownParser = (d) => {
  const flat = flatDeep(d, Infinity);
  const decoration = (flat.length <= 2 ? '' : flat[1]);
  const subject = (flat.length === 2 ? flat[1] : flat[2]) || '';
  const discussion = flat.slice(3).filter(d => d);

  return {
    "label": flat[0],
    "decoration": decoration,
    "subject": subject.trim(),
    "discussion": discussion,
  };
}
var grammar = {
    Lexer: undefined,
    ParserRules: [
    {"name": "_$ebnf$1", "symbols": []},
    {"name": "_$ebnf$1", "symbols": ["_$ebnf$1", "wschar"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "_", "symbols": ["_$ebnf$1"], "postprocess": function(d) {return null;}},
    {"name": "__$ebnf$1", "symbols": ["wschar"]},
    {"name": "__$ebnf$1", "symbols": ["__$ebnf$1", "wschar"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "__", "symbols": ["__$ebnf$1"], "postprocess": function(d) {return null;}},
    {"name": "wschar", "symbols": [/[ \t\n\v\f]/], "postprocess": id},
    {"name": "comment", "symbols": ["L", "_", "decoration", "S", "DI"], "postprocess": commentParser},
    {"name": "comment", "symbols": ["LM", "S", "DI"], "postprocess": markdownParser},
    {"name": "L$subexpression$1", "symbols": [/[cC]/, /[hH]/, /[oO]/, /[rR]/, /[eE]/], "postprocess": function(d) {return d.join(""); }},
    {"name": "L", "symbols": ["L$subexpression$1"]},
    {"name": "L$subexpression$2", "symbols": [/[iI]/, /[sS]/, /[sS]/, /[uU]/, /[eE]/], "postprocess": function(d) {return d.join(""); }},
    {"name": "L", "symbols": ["L$subexpression$2"]},
    {"name": "L$subexpression$3", "symbols": [/[nN]/, /[iI]/, /[tT]/, /[pP]/, /[iI]/, /[cC]/, /[kK]/], "postprocess": function(d) {return d.join(""); }},
    {"name": "L", "symbols": ["L$subexpression$3"]},
    {"name": "L$subexpression$4", "symbols": [/[pP]/, /[rR]/, /[aA]/, /[iI]/, /[sS]/, /[eE]/], "postprocess": function(d) {return d.join(""); }},
    {"name": "L", "symbols": ["L$subexpression$4"]},
    {"name": "L$subexpression$5", "symbols": [/[qQ]/, /[uU]/, /[eE]/, /[sS]/, /[tT]/, /[iI]/, /[oO]/, /[nN]/], "postprocess": function(d) {return d.join(""); }},
    {"name": "L", "symbols": ["L$subexpression$5"]},
    {"name": "L$subexpression$6", "symbols": [/[sS]/, /[uU]/, /[gG]/, /[gG]/, /[eE]/, /[sS]/, /[tT]/, /[iI]/, /[oO]/, /[nN]/], "postprocess": function(d) {return d.join(""); }},
    {"name": "L", "symbols": ["L$subexpression$6"]},
    {"name": "L$subexpression$7", "symbols": [/[tT]/, /[hH]/, /[oO]/, /[uU]/, /[gG]/, /[hH]/, /[tT]/], "postprocess": function(d) {return d.join(""); }},
    {"name": "L", "symbols": ["L$subexpression$7"]},
    {"name": "LM", "symbols": ["MD", "L", "_", "decoration"], "postprocess": d => [d[1][0], d[3][0]]},
    {"name": "LM", "symbols": ["MD", "L", "MD", "_", "decoration"], "postprocess": d => [d[1], d[4]]},
    {"name": "MD", "symbols": ["BC"], "postprocess": emptyStr},
    {"name": "MD", "symbols": ["IC"], "postprocess": emptyStr},
    {"name": "BC$string$1", "symbols": [{"literal":"*"}, {"literal":"*"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "BC", "symbols": ["BC$string$1"]},
    {"name": "IC$string$1", "symbols": [{"literal":"_"}, {"literal":"_"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "IC", "symbols": ["IC$string$1"]},
    {"name": "decoration", "symbols": ["colon"]},
    {"name": "decoration", "symbols": [{"literal":"("}, "_", "D", "_", {"literal":")"}, "colon"], "postprocess": d => d[2]},
    {"name": "decoration", "symbols": [{"literal":"("}, "_", "D", "_", {"literal":")"}, "MD", "colon"], "postprocess": d => d[2]},
    {"name": "decoration", "symbols": [{"literal":"("}, "_", "D", "_", {"literal":")"}, "colon", "MD"], "postprocess": d => d[2]},
    {"name": "decoration", "symbols": ["MD", {"literal":"("}, "_", "D", "_", {"literal":")"}, "colon", "MD"], "postprocess": d => d[3]},
    {"name": "decoration", "symbols": ["MD", {"literal":"("}, "_", "D", "_", {"literal":")"}, "MD", "colon"], "postprocess": d => d[3]},
    {"name": "D$string$1", "symbols": [{"literal":"n"}, {"literal":"o"}, {"literal":"n"}, {"literal":"-"}, {"literal":"b"}, {"literal":"l"}, {"literal":"o"}, {"literal":"c"}, {"literal":"k"}, {"literal":"i"}, {"literal":"n"}, {"literal":"g"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "D", "symbols": ["D$string$1"]},
    {"name": "S", "symbols": ["single_line"]},
    {"name": "DI$ebnf$1", "symbols": []},
    {"name": "DI$ebnf$1$subexpression$1", "symbols": ["newline", "para"]},
    {"name": "DI$ebnf$1", "symbols": ["DI$ebnf$1", "DI$ebnf$1$subexpression$1"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "DI", "symbols": ["DI$ebnf$1"], "postprocess": id},
    {"name": "single_line", "symbols": ["para", "char"], "postprocess": appendItemChar(0,1)},
    {"name": "para", "symbols": [], "postprocess": emptyStr},
    {"name": "para", "symbols": ["para", "char"], "postprocess": appendItemChar(0,1)},
    {"name": "char", "symbols": [/[^*\n|*\r]/], "postprocess": id},
    {"name": "newline", "symbols": [{"literal":"\r"}, {"literal":"\n"}], "postprocess": emptyStr},
    {"name": "newline", "symbols": [{"literal":"\r"}]},
    {"name": "newline", "symbols": [{"literal":"\n"}], "postprocess": emptyStr},
    {"name": "colon", "symbols": [{"literal":":"}], "postprocess": emptyStr}
]
  , ParserStart: "comment"
}
if (typeof module !== 'undefined'&& typeof module.exports !== 'undefined') {
   module.exports = grammar;
} else {
   window.grammar = grammar;
}
})();
