export interface ParseResult {
  label: string | 'chore' | 'issue' | 'nitpick' | 'praise' | 'question' | 'suggestion' | 'thought';
  decoration: string;
  subject: string;
  discussion: string[];
}
