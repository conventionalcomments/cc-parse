import { ParseResult } from '../models';

type MockParse = {
  comment: string;
  expected: ParseResult;
};

type MockComment = {
  label?: string;
  decoration?: string;
  subject?: string;
  discussion?: string[];
};

export const buildMockParse = (
  label: string = 'question',
  decoration: string = '',
  subject: string = 'this is the subject',
  discussion: string[] = [],
): MockParse => {
  let comment = `${label}`;

  if (decoration && !decoration.includes('(')) {
    comment += ` (${decoration})`;
  } else if (decoration) {
    comment += ` ${decoration}`;
  }

  comment += `: `;

  if (subject) {
    comment += subject;
  }

  discussion.forEach((x) => {
    comment += `\n${x}`;
  });

  return {
    comment,
    expected: {
      label,
      decoration: decoration.trim(),
      subject,
      discussion,
    },
  };
};

export const buildWithMarkdown = (mock: MockComment) => {
  const { label = 'question', decoration = '', subject, discussion = [] } = mock;

  const labelMd = `**${label}**`;
  const decorationMd = decoration ? `**(${decoration})**` : decoration;
  const subjectMd = subject ? `__${subject}__` : '__this is the subject__';
  const discussionMd = discussion.map((x) => `**${x}**`);

  const { comment } = buildMockParse(labelMd, decorationMd, subjectMd, discussionMd);

  return {
    comment,
    expected: {
      label,
      decoration,
      subject: '__this is the subject__',
      discussion,
    },
  };
};
