import parse from '../';
import { buildMockParse, buildWithMarkdown } from './helpers';

describe('parser', () => {
  describe('basic tests', () => {
    describe('labels', () => {
      describe.each`
        label
        ${'praise'}
        ${'nitpick'}
        ${'suggestion'}
        ${'issue'}
        ${'question'}
        ${'thought'}
        ${'chore'}
      `('parsing label', ({ label }) => {
        test(`returns ${label}`, () => {
          const { comment, expected } = buildMockParse(label);

          expect(parse(comment)).toEqual(expected);
        });
      });

      describe('does not parse unknown labels', () => {
        it('returns null by default', () => {
          const { comment } = buildMockParse('foo');

          expect(parse(comment)).toBe(null);
        });

        it('throws when specified', () => {
          const { comment } = buildMockParse('foo');

          expect(() => parse(comment, true)).toThrow(Error);
        });
      });
    });

    describe('decorations', () => {
      it('works with basic decorations', () => {
        const { comment, expected } = buildMockParse('nitpick', 'non-blocking');

        expect(parse(comment)).toEqual(expected);
      });

      it('works with weird decorators', () => {
        const { comment, expected } = buildMockParse('nitpick', '  non-blocking  ');

        expect(parse(comment)).toEqual(expected);
      });
    });

    describe('subject', () => {
      it('returns the full subject', () => {
        const { comment, expected } = buildMockParse();
        const result = parse(comment);

        expect(result.subject).toEqual(expected.subject);
      });

      describe('does not parse without a subject', () => {
        it('returns null by default', () => {
          const { comment } = buildMockParse('question', '', '');

          expect(parse(comment)).toBe(null);
        });

        it('throws when specified', () => {
          const { comment } = buildMockParse('question', '', '');

          expect(() => parse(comment, true)).toThrow(Error);
        });
      });
    });

    describe('discussions', () => {
      it('returns a full discussion', () => {
        const discussion = ['this is the first line'];
        const { comment, expected } = buildMockParse('question', '', 'a subject', discussion);

        expect(parse(comment)).toEqual(expected);
      });

      it('returns a discussion spanning multiple paragraphs', () => {
        const discussion = ['one line', 'two line'];
        const { comment, expected } = buildMockParse('question', '', 'a subject', discussion);
        const result = parse(comment);

        expect(result).toEqual(expected);
      });
    });
  });

  describe('with markdown', () => {
    describe('labels and decorators', () => {
      it('works with comment and decoration', () => {
        const { comment, expected } = buildWithMarkdown({ decoration: 'non-blocking' });
        const result = parse(comment);

        expect(result).toEqual(expected);
      });

      it('works with italics', () => {
        const comment = '__nitpick__ __(non-blocking)__: this is the subject';
        const expected = {
          label: 'nitpick',
          decoration: 'non-blocking',
          subject: 'this is the subject',
          discussion: [],
        };
        const result = parse(comment);

        expect(result).toEqual(expected);
      });

      it('works as single line', () => {
        const comment = '**nitpick (non-blocking):** this is the subject';
        const expected = {
          label: 'nitpick',
          decoration: 'non-blocking',
          subject: 'this is the subject',
          discussion: [],
        };
        const result = parse(comment);

        expect(result).toEqual(expected);
      });
    });

    describe('in the discussion', () => {
      it('preserves markdown', () => {
        const comment = `**nitpick (non-blocking):** this is the subject

a line without markdown
__a line with markdown__`;
        const expected = {
          label: 'nitpick',
          decoration: 'non-blocking',
          subject: 'this is the subject',
          discussion: ['a line without markdown', '__a line with markdown__'],
        };
        const result = parse(comment);

        expect(result).toEqual(expected);
      });
    });
  });
});
